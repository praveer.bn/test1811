package com.entity;

import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "aid", sequenceName = "address_sqc")
    private  int aid;
    private  String city;
    private int flatNo;

    public Address() {
    }

    public Address(String city, int flatNo) {
        this.city = city;
        this.flatNo = flatNo;
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(int flatNo) {
        this.flatNo = flatNo;
    }

    @Override
    public String toString() {
        return "Address{" +
                "aid=" + aid +
                ", city='" + city + '\'' +
                ", flatNo=" + flatNo +
                '}';
    }
}
