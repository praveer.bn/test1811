package com.entity;

import javax.persistence.*;

@Entity
public class Student {
    @Id
    private  int sid;
    private String sName;
//    private  Address address;

    public Student() {
    }

    public Student(int sid, String sName) {
        this.sid = sid;
        this.sName = sName;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

//    public Address getAddress() {
//        return address;
//    }
//
//    public void setAddress(Address address) {
//        this.address = address;
//    }

    @Override
    public String toString() {
        return "Student{" +
                "sid=" + sid +
                ", sName='" + sName + '\'' +

                '}';
    }
}
