package com.dao;

import com.entity.Student;

import java.sql.SQLException;
import java.util.List;

public interface Dao {
     List<Student> findAll() throws SQLException;
     public Student findById(int id) throws SQLException;
     void deleteById(int id) throws SQLException;

     void deleteAll() throws SQLException;

     void save(Student student) throws SQLException;





}
