package com.dao;

import com.connection.FactoryProvider;
import com.entity.Student;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DaoImpl implements Dao{
    public List<Student> findAll() throws SQLException {
        Connection connection = FactoryProvider.connect();
        Statement statement=connection.createStatement();
        ResultSet resultSet=statement.executeQuery("select * from student");
        List<Student> studentList = new ArrayList<Student>();

        while (resultSet.next()) {
            Student student = new Student();
            student.setSid(resultSet.getInt(1));
            student.setsName(resultSet.getString(2));
            studentList.add(student);
        }
        return studentList;
    }

    public Student findById(int id) throws SQLException {
        Connection connection = FactoryProvider.connect();
        ResultSet resultSet ;
        PreparedStatement preparedStatement = connection.prepareStatement("select * from Student " +
                "where sid = ?");
        preparedStatement.setInt(1,id);

        resultSet = preparedStatement.executeQuery();
        Student student = new Student();

        while (resultSet.next()) {
            student.setSid(resultSet.getInt(1));
            student.setsName(resultSet.getString(2));
        }
        return student;
    }

    @Override
    public void deleteById(int id) throws SQLException {
        Connection connection = FactoryProvider.connect();
        PreparedStatement statement=connection.prepareStatement("delete from student where sid="+id);
        statement.executeUpdate();

    }


    @Override
    public void deleteAll() throws SQLException {
        Connection connection = FactoryProvider.connect();
        PreparedStatement statement= connection.prepareStatement("delete from student");
        statement.executeUpdate();
    }

    @Override
    public void save(Student student) throws SQLException {
        Connection connection = FactoryProvider.connect();
        Statement statement=connection.createStatement();
        ResultSet resultSet=statement.executeQuery("Select * from student ");
        int count=0;

        while(resultSet.next())
        {
            if(student.getSid()==resultSet.getInt(1))
            {
                count=1;
                break;
            }

        }
        if(count==0) {

            PreparedStatement preparedStatement = connection.prepareStatement("Insert into student values (?,?)");
            preparedStatement.setInt(1, student.getSid());
            preparedStatement.setString(2, student.getsName());
            preparedStatement.execute();
            System.out.println("The student inserted successfully");
        }
        else {
            PreparedStatement preparedStatement = connection.prepareStatement("Update Student set sName=? where sid="+student.getSid());
            preparedStatement.setString(1, student.getsName());
            preparedStatement.execute();
            System.out.println("The student updated successfully");
        }
    }

}
