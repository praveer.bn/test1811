package com.connection;

import com.dao.DaoImpl;
import com.entity.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

public class Main {
    static BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
    static boolean flag=true;

    public static void main(String[] args) throws IOException, SQLException {


        while(flag) {

            DaoImpl dao = new DaoImpl();
            System.out.println("1-For adding new student");
            System.out.println("2-For findingAll");
            System.out.println("3-For finding by id");
            System.out.println("4-For delete by id");
            System.out.println("5-for delete");
            System.out.println("6-for exit");
            System.out.println("Enter the option you want");

            int option = Integer.parseInt(bufferedReader.readLine());

            switch (option) {
                case 1:
                    System.out.println("Enter the student id");
                    int id=Integer.parseInt(bufferedReader.readLine());
                    System.out.println("Enter the student name");
                    String name=bufferedReader.readLine();
                    Student student=new Student(id,name);
                    dao.save(student);
                    break;

                case 2:
                    List<Student> students = dao.findAll();
                    System.out.println(students.size());
                    System.out.println("The list of the students is as follows");
                    students.stream().forEach(System.out::println);
                    break;

                case 3:
                    System.out.println("Enter the id of the student you want to find information of");
                    int id1=Integer.parseInt(bufferedReader.readLine());
                    Student student1 = dao.findById(id1);
                    System.out.println("The student is "+student1);
                    break;

                case 4:
                    System.out.println("Enter the id of the student you want to delete");
                    int id2=Integer.parseInt(bufferedReader.readLine());
                    dao.deleteById(id2);
                    break;

                case 5:
                    System.out.println("Delete all the information");
                    dao.deleteAll();
                    break;
                case 6:
                  flag=false;
                    break;
                default:
                    System.out.println("wrong input");
            }


        }
    }
}





















